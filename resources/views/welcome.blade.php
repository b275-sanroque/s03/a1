{{-- Activity for s02 --}}

@extends('layouts.app')

@section('content')

  <div> 
    <img class="img-fluid" src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png">
    <h3 class="mt-5 text-center">FEATURED POSTS:</h3>
  </div>

  @if(count($posts) > 0)
    @foreach($posts->random(3) as $post)
      <div class="card text-center m-3">
          <div class="card-body">
            <h3 class="card-title">
                <a href="/posts/{{$post->id}}">{{$post->title}}</a>
            </h4>
            <p class="card-subtitle"> Author: {{$post->user->name}} </p>
          </div>
      </div>       
    @endforeach
  @endif

@endsection